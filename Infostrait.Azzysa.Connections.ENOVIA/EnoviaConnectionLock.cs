﻿using System.Net;
using Infostrait.Azzysa.Interfaces;

namespace Infostrait.Azzysa.Connections.ENOVIA
{
    /// <summary>
    /// IConnectionLock that is returned from <see cref="EnoviaConnectionPool.GetPoolObject(string,string,string,string)" />
    /// </summary>
    public class EnoviaConnectionLock : IConnectionLock
    {
        // Private members
        internal EnoviaConnection Connection;
        internal EnoviaConnectionPool Pool;
        
        /// <summary>
        /// Returns the connected EnoviaConnection object
        /// </summary>
        /// <returns></returns>
        IConnection IConnectionLock.GetConnection()
        {
            return Connection;
        }

        /// <summary>
        /// Returns the cookie container holding the session cookies to pass to other ENOVIA web service invocation objects
        /// </summary>
        /// <returns></returns>
        public CookieContainer GetCookieContainer()
        {
            return Connection.GetCookieContainer();
        }

        /// <summary>
        /// Returns the ENOVIA session id
        /// </summary>
        /// <returns></returns>
        public string GetSessionId()
        {
            return Connection.SessionId;
        }

        /// <summary>
        /// Unlocks the current connection making this object unavailable
        /// </summary>
        public void Unlock()
        {
            // Unregister the lock
            Connection.Unlock(this, Pool);
            
            // Make this object unavailable by resetting the members
            if (!DisposeConnectionOnDispose) { Connection = null; }
            Pool = null;

            // Finished
        }

        /// <summary>
        /// Gets or sets the value indicating whether to dispose the <see cref="EnoviaConnection"/> object when disposing this object
        /// </summary>
        public bool DisposeConnectionOnDispose { get; set; }

        /// <summary>
        /// Unlocks this lock, and releasing the ENOVIA connection to the pool to be available again
        /// </summary>
        public void Dispose()
        {
            // Unlock the object
            Unlock();

            // Check whether to dispose the ENOVIA connection as well
            if (DisposeConnectionOnDispose) { Connection?.Dispose();}
        }
    }
}