﻿using System;
using System.Linq;
using System.Net;
using Infostrait.Azzysa.Connectors.ENOVIA;
using Infostrait.Azzysa.Interfaces;

namespace Infostrait.Azzysa.Connections.ENOVIA
{

    /// <summary>
    /// IConnection implementation that supports maintaining a connection (ENOVIA session) with the platform
    /// </summary>
    public class EnoviaConnection : IConnection
    {

        // Private members
        //private ExtendedWebClient _client;
        internal string SessionId;
        internal string jSessionId;
        internal string lBCookie;
        private object _connectionLock;
        private EnoviaConnectionLock _lockedBy;
        private string _cachedUserName;
        private CookieContainer _cookieContainer;

        /// <summary>
        /// Internal delegate to support the on disposing event
        /// </summary>
        /// <param name="disposedConnection"></param>
        internal delegate void OnDisposingEventHandler(EnoviaConnection disposedConnection);

        /// <summary>
        /// Returns the cached user name (connection was last connected using this user)
        /// </summary>
        /// <returns></returns>
        internal string GetCachedUserName()
        {
            return string.IsNullOrEmpty(_cachedUserName) ? string.Empty : _cachedUserName;
        }

        /// <summary>
        /// On Dispose Event (invoke when the connection is going to be disposed)
        /// </summary>
        internal event OnDisposingEventHandler OnDisposing;

        /// <summary>
        /// Internal use only, default constructor initializing private members only..
        /// </summary>
        internal EnoviaConnection()
        {
            // Default constructor
            _connectionLock = new object();
        }

        /// <summary>
        /// Makes sure the ENOVIA connection is logged in with the specified user name, if this is not the case, the method will login in using the specified credentials
        /// </summary>
        /// <param name="servicesUrl">Base URL of ENOVIA services page where azzysa services are deployed</param>
        /// <param name="userName">User name</param>
        /// <param name="password">Password</param>
        /// <param name="vault">ENOVIA vault to log on to</param>
        internal void EnsureConnected(string servicesUrl, string userName, string password, string vault)
        {
            // First check whether there is still an active matrix.db.Context set for the HttpSession
            var serviceUrl = string.Format("{0}AzzysaServiceModeler/login/validate", (servicesUrl.EndsWith("/") ? servicesUrl : servicesUrl + "/"));

            // Check header count (content-type and accept header may reset after invoking web api)
            var _client = GetWebClient();
            var validSession = bool.Parse(_client.DownloadString(serviceUrl));
    
            // Check the user name
            if (!string.Equals(_cachedUserName, userName) || !validSession)
            {
                // Connected with a different user name, reconnect with specified credentials
                Connect(servicesUrl, userName, password, vault);
            }
        }
        
        /// <summary>
        /// Performs the actual login on the ENOVIA platform
        /// </summary>
        /// <param name="servicesUrl">Base URL of ENOVIA services page where azzysa services are deployed</param>
        /// <param name="userName">User name</param>
        /// <param name="password">Password of the user</param>
        /// <param name="vault">ENOVIA vault to log on to</param>
        /// <returns></returns>
        internal string Connect(string servicesUrl, string userName, string password, string vault)
        {
            lock (_connectionLock)
            {
                // Connect to ENOVIA RestService to authenticate with ENOVIA
                var _client = GetWebClient();
                var serviceUrl = string.Format("{0}AzzysaServiceModeler/login/authenticate/{1}/{2}/{3}", (servicesUrl.EndsWith("/") ? servicesUrl : servicesUrl + "/"), 
                    userName, password, vault);
                if (!bool.Parse(_client.DownloadString(serviceUrl)))
                {
                    // Login failed
                    throw new UnauthorizedAccessException($"Login failed for user {userName} and vault {vault}");
                }

                // Get cookie from response
                jSessionId = _client.ResponseHeaders["set-cookie"].Split(new char[] { Convert.ToChar(";"), Convert.ToChar(",") },
                    StringSplitOptions.RemoveEmptyEntries).FirstOrDefault(c => c.StartsWith("JSESSIONID=", StringComparison.InvariantCultureIgnoreCase));
                lBCookie = _client.ResponseHeaders["set-cookie"].Split(new char[] { Convert.ToChar(";"), Convert.ToChar(",") },
                    StringSplitOptions.RemoveEmptyEntries).FirstOrDefault(c => c.StartsWith("BIGipServer", StringComparison.InvariantCultureIgnoreCase));
                SessionId = jSessionId?.Split(new char[] {Convert.ToChar("=")}, StringSplitOptions.RemoveEmptyEntries).Last();

                // Initiate cookie container with the session cookie (used for SOAP/JPO-PROGRAM based services)
                _cookieContainer = new CookieContainer();
                var cookieObject = new Cookie("JSESSIONID", SessionId, "/") {Domain = new Uri(serviceUrl).Host};
                _cookieContainer.Add(cookieObject);

                // Finished, return session id as the result
                _cachedUserName = string.Copy(userName);
                return SessionId;
            }
        }

        /// <summary>
        /// Internal use, returns whether the connection is currently not locked by a process and therefore available for other process to be locked
        /// </summary>
        /// <returns></returns>
        internal bool IsAvailable()
        {
            lock (_connectionLock)
            {
                return _lockedBy == null;
            }
        }

        /// <summary>
        /// Lockes this connection object exclusively to the specified lock object
        /// </summary>
        /// <param name="pool">EnoviaConnectionPool object to register the new connection lock</param>
        internal EnoviaConnectionLock Lock(EnoviaConnectionPool pool)
        {
            lock (_connectionLock)
            {
                if (_lockedBy == null)
                {
                    // New lock
                    _lockedBy = new EnoviaConnectionLock
                    {
                        Connection = this,
                        Pool = pool
                    };
                    pool.RegisterLockObject(_lockedBy);

                    // Finished locking the object
                    return _lockedBy;
                }
                else
                {
                    // Already locked by another object
                    throw new InvalidOperationException("Current connection is already locked");
                }
            }
        }

        /// <summary>
        /// Unlock this connection to be used from within the pool
        /// </summary>
        /// <param name="lockObject"></param>
        internal void Unlock(EnoviaConnectionLock lockObject, EnoviaConnectionPool pool)
        {
            lock (_connectionLock)
            {
                if (_lockedBy == null)
                {
                    // Not locked..
                }
                else
                {
                    // Check if the lock object is the same as the current lock object
                    if (object.Equals(lockObject, _lockedBy))
                    {
                        // Same lock object, unlock now
                        lockObject.Connection = null;
                        lockObject.Pool = null;
                        pool.UnRegisterLockObject(lockObject);
                        _lockedBy = null;
                    }
                    else
                    {
                        // Different lock objects, already locked by another object
                        throw new InvalidOperationException("Current connection is already locked by a different object");
                    }
                }
            }
        }

        /// <summary>
        /// Returns the cookie container holding the session cookies to pass to other ENOVIA web service invocation objects
        /// </summary>
        /// <returns></returns>
        public CookieContainer GetCookieContainer()
        {
            return _cookieContainer;
        }
        
        /// <summary>
        /// Closes the connection with ENOVIA by disposing login connector, cookie containers and the remaining private members
        /// </summary>
        public void Dispose()
        {
            // Check for disposing event handler (registered by the ConnectionPool)
            OnDisposing?.Invoke(this);

            // Clean up private members
            SessionId = null;
            _connectionLock = null;
            _lockedBy = null;
            _cachedUserName = null;
        }

        /// <summary>
        /// Returns the initialized web client
        /// </summary>
        /// <returns></returns>
        public WebClient GetWebClient()
        {
            // Initiate ExtendedWebClient object (derived from System.net.WebClient in order to be able to modify request Timeout)
            var _client = new ExtendedWebClient
            {
                Headers =
                    {
                        [HttpRequestHeader.Accept] = "application/json",
                        [HttpRequestHeader.ContentType] = "application/json"
                    }
            };
            
            // Check whether to set session/load-balancer cookies
            if (!string.IsNullOrWhiteSpace(jSessionId))
            {
                if (string.IsNullOrWhiteSpace(lBCookie))
                {
                    _client.Headers.Add(HttpRequestHeader.Cookie, $"{jSessionId}");
                } else
                {
                    _client.Headers.Add(HttpRequestHeader.Cookie, $"{jSessionId};{lBCookie}");
                }
            }

            return _client;
        }
    }
}