﻿using System;
using System.Collections.Generic;
using System.Linq;
using Infostrait.Azzysa.Interfaces;

namespace Infostrait.Azzysa.Connections.ENOVIA
{

    /// <summary>
    /// IConnectionPool that hosts all connections with the ENOVIA platform, use this pool to create and lock new IConnection objects
    /// </summary>
    public class EnoviaConnectionPool : IConnectionPool
    {

        // Private members
        private object _poolLock;
        private object _registerLock;
        private List<EnoviaConnection> _poolConnections;
        private List<EnoviaConnectionLock> _connectionLocks; 

        /// <summary>
        /// Create a new connection pool, initializes but not creates and pool connection
        /// </summary>
        public EnoviaConnectionPool()
        {
            _poolLock = new object();
            _registerLock = new object();
            _poolConnections = new List<EnoviaConnection>();
            _connectionLocks = new List<EnoviaConnectionLock>();
        }

        /// <summary>
        /// Returns the number of initialized pool objects
        /// </summary>
        /// <returns></returns>
        public int GetPoolCount()
        {
            return _poolConnections.Count();
        }

        /// <summary>
        /// Gets the number of pool objects available or not (depending on the IsAvailable parameter)
        /// </summary>
        /// <param name="isAvailable">True to return the number of available objects, false to return the number of in used objects</param>
        /// <returns></returns>
        public int GetAvailableCount(bool isAvailable)
        {
            // Count the number according to the parameter
            return (from p in _poolConnections where p.IsAvailable() == isAvailable select p).Count();
        }

        /// <summary>
        /// Returns an EnoviaConnection pool that is available from the internal pool, or create a new one if not available and lock it exclusively to the caller
        /// </summary>
        /// <param name="servicesUrl">Base url to web services, login service URL is constructed based on this url</param>
        /// <param name="userName">User name to connect with (is checked after getting an existing connection from the pool)</param>
        /// <param name="password">Password for the specified user</param>
        /// <param name="vault">Name of the vault to specify on the login procedure</param>
        /// <returns></returns>
        public IConnectionLock GetPoolObject(string servicesUrl, string userName, string password, string vault)
        {
            lock (_poolLock)
            {
                // Try to get an available pool object from the internal collection already logged in with the specified user
                var poolConnection = (from p in _poolConnections where p.IsAvailable() && string.Equals(p.GetCachedUserName(), userName) select p).FirstOrDefault();
                
                // Check if an available object has been retrieved logged in by the specified user
                if (poolConnection == null)
                {
                    // No connection with the specified user available, get any connection available
                    poolConnection = (from p in _poolConnections where p.IsAvailable() select p).FirstOrDefault();
                }

                // Check if an available object has been retrieved
                if (poolConnection != null)
                {
                    // Make sure the connection is logged in with required credentials
                    poolConnection.EnsureConnected(servicesUrl, userName, password, vault);
                }
                else
                {
                    // Create a new EnoviaConnection and lock that connection
                    poolConnection = new EnoviaConnection();

                    // Register event handler for disposing the connection
                    poolConnection.OnDisposing += OnConnectionDisposed;

                    // Login to this connection and add to internal connection pool
                    poolConnection.Connect(servicesUrl, userName, password, vault);
                    _poolConnections.Add(poolConnection);
                }

                // Lock this object and return the locked connection
                return poolConnection.Lock(this);
            }
        }
        
        /// <summary>
        /// Internal use only, register a new connection lock
        /// </summary>
        /// <param name="lockObject"></param>
        internal void RegisterLockObject(EnoviaConnectionLock lockObject)
        {
            // First check the lock parameter
            if (lockObject == null) throw new ArgumentNullException("lockObject");
            lock (_registerLock)
            {
                // Check if it not already registered
                var foundLocks = from l in _connectionLocks where object.Equals(l, lockObject) select l;
                if (!foundLocks.Any())
                {
                    // As expected, register new lock
                    _connectionLocks.Add(lockObject);
                }
                else
                {
                    // Already locked, unexpected operation
                    throw new InvalidOperationException("Object lock already registered");
                }
            }
        }

        /// <summary>
        /// Internal use only, unregister an existing connection lock
        /// </summary>
        /// <param name="lockObject"></param>
        internal void UnRegisterLockObject(EnoviaConnectionLock lockObject)
        {
            // First check the lock parameter
            if (lockObject == null) throw new ArgumentNullException("lockObject");
            lock (_registerLock)
            {
                // Check if it not already registered
                var foundLock = (from l in _connectionLocks where object.Equals(l, lockObject) select l).FirstOrDefault();
                if (foundLock != null)
                {
                    // As expected, unregister new lock
                    _connectionLocks.Remove(lockObject);
                }
                else
                {
                    // Already locked, unexpected operation
                    throw new InvalidOperationException("Object lock not registered");
                }
            }
        }

        /// <summary>
        /// Internal use, is triggered when an <see cref="IConnection"/> object is disposed.
        /// Object will be removed from the internal pool collection
        /// </summary>
        /// <param name="disposedConnection"></param>
        internal void OnConnectionDisposed(EnoviaConnection disposedConnection)
        {
            if (disposedConnection != null)
            {
                _poolConnections.Remove(disposedConnection);
            }
        }

        /// <summary>
        /// Closes the connection pool by unlocking all locked connections and finally closes all connections with the platform
        /// </summary>
        public void Dispose()
        {
            // Unlock all lock objects
            for (var idx = _connectionLocks.Count - 1; idx >= 0; idx--)
            {
                var lockObject = _connectionLocks[idx];
                // Do not dispose connection when disposing/unlocking the locks in order to keep control in the next loop in this method (Remove all connection objects)
                lockObject.DisposeConnectionOnDispose = false;
                lockObject.Unlock();
                lockObject = null;
                _connectionLocks.RemoveAt(idx);
            }
            _connectionLocks = null;

            // Remove all connection objects
            for (var idx = _poolConnections.Count - 1; idx >= 0; idx--)
            {
                var connection = _poolConnections[idx];

                // Remove all event handler to prevent eternal loops
                connection.OnDisposing -= OnConnectionDisposed;

                // Dispose the connection
                connection.Dispose();
                connection = null;
                _poolConnections.RemoveAt(idx);
            }
            _poolConnections = null;

            _poolLock = null;
        }
    }
}