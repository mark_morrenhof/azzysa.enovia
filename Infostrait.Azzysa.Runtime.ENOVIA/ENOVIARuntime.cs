﻿using Infostrait.Azzysa.Connections.ENOVIA;
using Infostrait.Azzysa.Connectors.ENOVIA;
using Infostrait.Azzysa.Interfaces;
using Infostrait.Azzysa.Providers.ENOVIA;

namespace Infostrait.Azzysa.Runtime.ENOVIA
{
    public class ENOVIARuntime : PoolRuntime
    {

        /// <summary>
        /// Invoke base class with the ENOVIA settings provider
        /// <param name="connectionString"></param>
        /// <param name="serverName">Set the name of the azzysa server</param>
        /// <param name="siteName">Set the name of the site (where this server is member of)</param>
        /// <param name="rootPath">Sets the azzysa platform root directory (used for determine persistent storage, temp storary and application contents)</param>
        /// <exception cref="System.ArgumentNullException">When nog settings provider is provided</exception>
        /// </summary>
        public ENOVIARuntime(string connectionString, string serverName, string siteName, string rootPath) : 
            base(new SettingsProvider(), connectionString, serverName, siteName, rootPath)
        {
        }

        /// <summary>
        /// Returns the <see cref="IExchangeFrameworkProvider"/> implementation that is applicable to the IRuntime implementation
        /// </summary>
        /// <param name="connectionLock"><see cref="IConnectionLock"/> implementation that holds the appropriate CookieContainer required to establish a connection</param>
        /// <returns></returns>
        public override IExchangeFrameworkProvider GetExchangeFrameworkProvider(IConnectionLock connectionLock)
        {
            return new ExchangeFrameworkConnector(this, connectionLock);
        }

        /// <summary>
        /// Clean up, connection pool and any other private member
        /// </summary>
        public override void Dispose()
        {
            // Dispose connection pool
            ConnectionPool?.Dispose();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EnoviaConnectionPool"/> object
        /// </summary>
        /// <returns></returns>
        protected override IConnectionPool InitializeConnectionPool()
        {
            // Return new instance of the EnoviaConnectionPool
            return new EnoviaConnectionPool();
        }
    }
}