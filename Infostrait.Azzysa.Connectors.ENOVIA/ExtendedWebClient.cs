﻿using System;
using System.Net;

namespace Infostrait.Azzysa.Connectors.ENOVIA
{

    /// <summary>
    /// Custom class that supports TimeOut settings (not applicable for the default <see cref="WebClient"/> class)
    /// </summary>
    public class ExtendedWebClient : WebClient
    {

        public TimeSpan TimeOut { get; set; }

        public ExtendedWebClient() : base()
        {
            TimeOut = TimeSpan.FromMinutes(2);
        }

        protected override WebRequest GetWebRequest(Uri uri)
        {
            var w = base.GetWebRequest(uri);
            if (w != null)
            {
                w.Timeout = TimeOut.Milliseconds > 0 ? TimeOut.Milliseconds : System.Threading.Timeout.Infinite;
            }
            return w;
        }

        public WebRequest GetRequest(Uri uri)
        {
            return GetWebRequest(uri);
        }
    }
}