﻿using System;
using System.IO;
using System.Net;
using System.Text;
using Infostrait.Azzysa.EF.Interfaces;
using Infostrait.Azzysa.Interfaces;
using Newtonsoft.Json;

namespace Infostrait.Azzysa.Connectors.ENOVIA
{
    
    /// <summary>
    /// Exchange Framework provider implementation for ENOVIA, sends <see cref="IDataPackageBase"/> packages to ENOVIA to process them
    /// </summary>
    public class ExchangeFrameworkConnector : IExchangeFrameworkProvider
    {
        
        // Private member(s)
        private readonly IRuntime _runtime;
        private readonly IConnectionLock _connectionLock;

        /// <summary>
        /// Constructs a new Exchange Framework
        /// </summary>
        /// <param name="runtime">Initialized <see cref="IRuntime"/> object</param>
        /// <param name="connectionLock"><see cref="IConnectionLock"/> implementation that holds the appropriate CookieContainer required to establish a connection</param>
        public ExchangeFrameworkConnector(IRuntime runtime, IConnectionLock connectionLock)
        {
           // Default constructor
            _runtime = runtime;
            _connectionLock = connectionLock;
        }
        
        /// <summary>
        /// Invokes the Exchange Framework web service and returns the returned (processed) package
        /// </summary>
        /// <param name="package">Data package holding the objects to process</param>
        /// <param name="configurationName">Name of the ENOVIA page object that holds the configuration</param>
        /// <returns></returns>
        public IDataPackageBase ProcessPackage(IDataPackageBase package, string configurationName)
        {
            // Convert package to JSON string
            var sb = new StringBuilder();
            var serializer = new Newtonsoft.Json.JsonSerializer();
             
            serializer.Converters.Add(new WholeNumberJsonConverter());

            using (var writer = new JsonTextWriter(new StringWriter(sb)))
                serializer.Serialize(writer, package);

            var jsonData = Encoding.UTF8.GetBytes(sb.ToString());

            // Initialize WebClient required to invoke Web Api
            var client = _connectionLock.GetConnection().GetWebClient() as ExtendedWebClient;
            if (client == null) { throw new ApplicationException("Unable to get initialized web client");}

            try
            {
                // Set TimeOut of the WebClient to 20 minutes during the invocation
                client.TimeOut = TimeSpan.FromMinutes(20);

                // Perform the actual invoke
                var serviceUrl = string.Format("{0}AzzysaServiceModeler/framework/process", (_runtime.ServicesUrl.EndsWith("/") ? _runtime.ServicesUrl : _runtime.ServicesUrl + "/"));
                var result = Encoding.UTF8.GetString(client.UploadData(serviceUrl, "POST", jsonData));

                // Succed, get the returned IDataPackageBase holding the process results
                var reader = new JsonTextReader(new StringReader(result));
                var returnPackage = (IDataPackageBase)serializer.Deserialize(reader, package.GetType());

                // Return package has been read into an in-memory object, return to caller
                return returnPackage;
            }
            catch (WebException ex)
            {
                var response = ex.Response;
                if (response != null)
                {
                    var responseStream = response.GetResponseStream();
                    if (responseStream != null)
                    {
                        var length = responseStream.Length;
                        var errData = new byte[length];
                        responseStream.Read(errData, 0, (int)length);
                        var errorText =
                            Encoding.UTF8.GetString(errData)
                                .Replace("\\r", Environment.NewLine)
                                .Replace("\\n", Environment.NewLine)
                                .Replace("\\t", "     ");

                        throw new ApplicationException(errorText);
                    }
                    else
                    {
                        throw;
                    }
                }
                else
                {
                    throw;
                }
            }
            finally
            {
                client.TimeOut = TimeSpan.FromMinutes(2);
                sb.Clear();
            }
        }
    }
}