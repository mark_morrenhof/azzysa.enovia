﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Infostrait.Azzysa.Interfaces;
using Newtonsoft.Json;
using System.Text;

namespace Infostrait.Azzysa.Providers.ENOVIA
{

    /// <summary>
    /// <see cref="ISettingsProvider"/> implementation that uses ENOVIA as the settings store
    /// </summary>
    public class SettingsProvider : SettingsProviderBase
    {

        /// <summary>
        /// Reads all settings from ENOVIA
        /// </summary>
        /// <param name="runtime">Reference to the <see cref="IRuntime"/> object that is hosting this ISettingsProvider</param>
        /// <param name="connectData">Connect information provided to the implementation to be able to read the settings</param>
        /// <returns></returns>
        public override ISettingsDictionary ReadSettings(IRuntime runtime, string connectData)
        {
            // Since 3DEXPERIENCE 2016x the (Azzysa)/ENOVIA service/api layer changed security policy
            // Therefore it is required to explicitly login
            if (string.IsNullOrWhiteSpace(runtime.UserName)) throw new ApplicationException("Unable to get login credentials in order to retrieve settings");

            var serviceUrl = string.Format("{0}AzzysaServiceModeler/login/authenticate/{1}/{2}/{3}", (runtime.ServicesUrl.EndsWith("/") ? runtime.ServicesUrl : runtime.ServicesUrl + "/"),
                runtime.UserName, runtime.Password, runtime.Vault);

            var client = new WebClient
            {
                Headers =
                    {
                        [HttpRequestHeader.Accept] = "application/json",
                        [HttpRequestHeader.ContentType] = "application/json"
                    }
            };

            if (!bool.Parse(client.DownloadString(serviceUrl)))
            {
                // Login failed
                throw new UnauthorizedAccessException($"Login failed for user {runtime.UserName} and vault {runtime.Vault}");
            }

            // Get all cookie from response
            var jSessionId = client.ResponseHeaders["set-cookie"].Split(new char[] { Convert.ToChar(";"), Convert.ToChar(",") },
                StringSplitOptions.RemoveEmptyEntries).FirstOrDefault(c => c.StartsWith("JSESSIONID=", StringComparison.InvariantCultureIgnoreCase));
            var lBCookie = client.ResponseHeaders["set-cookie"].Split(new char[] { Convert.ToChar(";"), Convert.ToChar(",") },
                StringSplitOptions.RemoveEmptyEntries).FirstOrDefault(c => c.StartsWith("BIGipServer", StringComparison.InvariantCultureIgnoreCase));

            // Reset WebClient with new cookie and request headers (may have been reset)
            client = new WebClient
            {
                Headers =
                    {
                        [HttpRequestHeader.Accept] = "application/json",
                        [HttpRequestHeader.ContentType] = "application/json"
                    }
            };
            // Set all required cookies (load balancer cookies, JSESSIONID, etc..)
            client.Headers.Add(HttpRequestHeader.Cookie, $"{jSessionId};{lBCookie}");

            // Get environmental settings
            var siteName = string.IsNullOrWhiteSpace(runtime.SiteName) ? "empty" : runtime.SiteName;
            var serverName = string.IsNullOrWhiteSpace(runtime.ServerName) ? "empty" : runtime.ServerName;
            
            // Connect to web service and return the settings dictionairy
            serviceUrl = string.Format("{0}AzzysaServiceModeler/settings/get/{1}/{2}", (runtime.ServicesUrl.EndsWith("/") ? runtime.ServicesUrl : runtime.ServicesUrl + "/"), siteName, serverName);
            var content = default(string);
            try
            {
                content = client.DownloadString(serviceUrl);
            } catch (WebException ex)
            {
                var response = ex.Response;
                if (response != null)
                {
                    var responseStream = response.GetResponseStream();
                    if (responseStream != null)
                    {
                        var length = responseStream.Length;
                        var errData = new byte[length];
                        responseStream.Read(errData, 0, (int)length);
                        var errorText =
                            Encoding.UTF8.GetString(errData)
                                .Replace("\\r", Environment.NewLine)
                                .Replace("\\n", Environment.NewLine)
                                .Replace("\\t", "     ");

                        throw new ApplicationException(errorText);
                    }
                    else
                    {
                        throw;
                    }
                }
                else
                {
                    throw;
                }
            } 

            // Since the requested SettingsDictionairy is not supported by this JSON serializer, use an intermediate format
            if (!string.IsNullOrWhiteSpace(content))
            {
                var r = new SettingsDictionary();
                JsonConvert.DeserializeObject<RootObject>(content).Setting?.ForEach(s => r.Add(s.Key, s.Value));
                return r;
            } else
            {
                // Invalid result provided SettingsProvider backend service
                throw new ApplicationException("Invalid result provided SettingsProvider backend service");
            }
        }

        /// <summary>
        /// Saves all settings from ENOVIA
        /// </summary>
        /// <param name="runtime">Reference to the <see cref="IRuntime"/> object that is hosting this ISettingsProvider</param>
        /// <param name="connectionString">Connect information provided to the implementation to be able to read the settings</param>
        /// <param name="settings">Settings to write</param>
        /// <remarks>Not implemented, no exception is thrown</remarks>
        public override void SaveSettings(IRuntime runtime, string connectionString, ISettingsDictionary settings)
        {
            // For now, do not save
        }
    }

    public class Setting
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }

    public class RootObject
    {
        private List<Setting> setting;
        
        public List<Setting> Setting {
            get { return setting; }
            set { setting = value; } }

        public List<Setting> Settings
        {
            get { return setting; }
            set { setting = value; }
        }

        public RootObject()
        {
            this.Setting = new List<Setting>();
        }
    }
}